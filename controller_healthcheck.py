# -*- coding: utf-8 -*-
from flask_restx import Resource, Namespace, fields

api = Namespace('healthcheck', description='healthcheck')

reponse_ok = api.model('Success', {
    'ok': fields.Boolean,
})
# -*- coding: utf-8 -*-
from flask_restx import Resource, Namespace, fields

api = Namespace('healthcheck', description='healthcheck')

reponse_ok = api.model('Success', {
    'ok': fields.Boolean,
})


@api.route('')
class HealthCheck(Resource):
    """
        healthCheck
        return:
         si ok {"ok": True}
         sinon code erreur
    """
    @api.response(200, 'Success', reponse_ok)
    def get(self):
        return {"ok": True}


@api.route('')
class HealthCheck(Resource):
    """
        healthCheck
        return:
         si ok {"ok": True}
         sinon code erreur
    """
    @api.response(200, 'Success', reponse_ok)
    def get(self):
        return {"ok": True}
