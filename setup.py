# -*- coding: utf-8 -*-
# build l'application :
# python setup.py install

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

version = '1.0.0'

setup(name='fdr_api_healthcheck',
      version=version,
      description="FDR Api healthcheck",
      author='DNUM/SDSI/BRR - Patrice Belmonte',
      url='',
      install_requires=requirements,
      packages=find_packages(),
      zip_safe=False)
